import os
from datetime import datetime
from OpenSSL import crypto as c


def ffname(fname):
    return os.path.join("/etc/certs", fname)


def is_expired(fname):
    cert = c.load_certificate(c.FILETYPE_PEM, open(ffname(fname)).read())
    expiry = datetime.strptime(cert.get_notAfter(), "%Y%m%d%H%M%SZ")
    return expiry < datetime.now()


for fname in os.listdir("/etc/certs"):
    if ".crt" in fname and is_expired(fname):
        print("removing", fname)
        os.remove(ffname)
