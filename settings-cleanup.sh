#!/bin/bash

# set default http password and basic system info
# resets eth0 to dhcp

rm -f /etc/nginx/.htpasswd
cp /etc/nginx/.htpasswd.default /etc/nginx/.htpasswd
chown grdw:grdw /etc/nginx/.htpasswd
(cd / && sudo -u grdw /home/grdw/.virtualenvs/gd3/bin/python /home/grdw/gd3_server/manage.py runscript settings_cleanup)
