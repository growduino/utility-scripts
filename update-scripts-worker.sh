#!/bin/bash
cd /root/utility-scripts/

#baliky co muzou chybet
apt update
apt-get --assume-yes dist-upgrade
apt-get --assume-yes install libzmq3-dev
apt-get --assume-yes purge rabbitmq*

#novy konfiguraky pro nginx
rm /etc/nginx/sites-available/*
cp /root/utility-scripts/etc/nginx/sites-available/* /etc/nginx/sites-available/
chgrp grdw /etc/nginx/sites-available
chmod g+w /etc/nginx/sites-available
touch /etc/nginx/sites-available/default-443-wlan0
touch /etc/nginx/sites-available/default-443-eth0

#clean up supervisor config
rm /etc/supervisor/conf.d/*

#django config
rsync -av /root/utility-scripts/django/ ~grdw/gd3_server/
#grdw user home
rsync -av /root/utility-scripts/grdw_home/ /home/grdw
#put everything under etc/ into /etc
rsync -av /root/utility-scripts/etc/ /etc
#motion config
rsync -av /root/utility-scripts/streamer/ /home/streamer
#put everything under bin/ into /usr/local/bin
rsync -av /root/utility-scripts/bin/ /usr/local/bin

ln -sf /etc/nginx/sites-available/default-443-wlan0 /etc/nginx/sites-enabled/
ln -sf /etc/nginx/sites-available/default-443-eth0 /etc/nginx/sites-enabled/
./set-ssl-redirect.sh
if [ ! -e /etc/nginx/.htpasswd ]; then
    cp /etc/nginx/.htpasswd.default /etc/nginx/.htpasswd
fi
chmod +x /etc/rc.local
chown -R grdw:grdw /etc/nginx/sites-available
chown -R grdw:grdw /etc/nginx/sites-enabled
chown -R grdw:grdw /etc/nginx/.htpasswd*
chown -R grdw:grdw /etc/certs
chown -R grdw:grdw /home/grdw

# reset NM to known state
rm -rf /etc/network/interfaces.d/*
cd ~grdw && sudo -u grdw /home/grdw/.virtualenvs/gd3/bin/python /home/grdw/gd3_server/manage.py runscript eth_setup
nmcli connection up grdw-ethernet
nmcli connection delete 'Ethernet connection 1' && /usr/sbin/service network-manager restart && echo "* * * * * * * * * * reboot needed * * * * * * * * * *"
nmcli connection up grdw-ethernet

# virtual Wire
for i in ~grdw/src/arduino*; do cp /root/utility-scripts/arduino/Wire.h ${i}/hardware/arduino/avr/libraries/Wire/src/Wire.h; done

# update autoupdate script
cp /root/utility-scripts/autoupdate* ~grdw/gd3_server/
chown -R grdw:grdw ~grdw
sudo -u grdw ~grdw/gd3_server/autoupdate.sh


#crontabs
crontab -u grdw /root/utility-scripts/crontabs/grdw
crontab /root/utility-scripts/crontabs/root

chown -R streamer:streamer /home/streamer

#fstab
if [ ! -d /mnt/sdcard ]; then mkdir /mnt/sdcard; fi

# tohle az nakonec, nastartuje supervisora
sudo -u grdw /usr/local/bin/new-addr.sh
