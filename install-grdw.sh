. /usr/share/virtualenvwrapper/virtualenvwrapper.sh
cd ~
git clone https://gitlab.com/growduino/gd3-server.git
mkdir ~/Arduino
mkdir ~/Arduino/libraries
cd ~/Arduino
git clone https://gitlab.com/growduino/GrowduinoFirmware3.git
mkdir ~/src
cd ~/src/
ARCHIV=arduino-1.8.13-linuxarm.tar.xz
wget https://downloads.arduino.cc/${ARCHIV}
tar xJvf ${ARCHIV}
ln -s arduino-1.8.13 arduino
rm ${ARCHIV}
cd ~
git clone https://gitlab.com/growduino/gd3-server.git
cd gd3-server
git checkout v3-master
mkvirtualenv -p `which python3` gd3
python3 -m pip install setuptools==59.5.0
mkdir /home/grdw/local_settings
add2virtualenv /home/grdw/local_settings
pip install -r environ/requirements.txt
cd ~
django-admin startproject gd3_server
mv gd3-server gd3_server/core
mkdir gd3_server/static
cd gd3_server/core/
git clone https://gitlab.com/growduino/frontend-pages.git static/
