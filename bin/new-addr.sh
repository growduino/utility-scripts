#!/bin/bash

#export here is needed for /etc/certs/openssl.conf
export IP=`ip addr show eth0 | grep "\<inet\>" | awk '{ print $2 }' | awk -F "/" '{ print $1 }'`
if [ -n "${IP}" ]; then  # eth0 is defined

    if [ ! -e /etc/nginx/sites-enabled/default-443-eth0 ]; then  # enable https on eth0
        ln -sf /etc/nginx/sites-available/default-443-eth0 /etc/nginx/sites-enabled/
    fi

    if [ ! -s /etc/certs/$IP.key ]; then  # re-gerate certificate for given IPaddr
        openssl req -config /etc/certs/openssl.conf -new -newkey rsa:4096 -x509 -sha256 -days 3650 -extensions req_ext -nodes -out /etc/certs/$IP.crt -keyout /etc/certs/$IP.key
    fi

    grep -q $IP /etc/nginx/sites-available/default-443-eth0 || ETHUPDATED=true  # fix nginx config for new IPaddr
    if [ ${ETHUPDATED} ]; then
        envsubst '${IP}' < /etc/nginx/sites-available/default.template > /etc/nginx/sites-available/default-443-eth0
        UPDATED=true
    fi
else
    rm -f /etc/nginx/sites-enabled/default-443-eth0  # eth0 not defined, disable in nginx
fi

export IP=`ip addr show wlan0 | grep "\<inet\>" | awk '{ print $2 }' | awk -F "/" '{ print $1 }'`
if [ -n "${IP}" ]; then 
    if [ ! -e /etc/nginx/sites-enabled/default-443-wlan0 ]; then
        ln -sf /etc/nginx/sites-available/default-443-wlan0 /etc/nginx/sites-enabled/
    fi
    if [ ! -e /etc/certs/$IP.key ]; then
        openssl req -config /etc/certs/openssl.conf -new -newkey rsa:4096 -x509 -sha256 -days 3650 -extensions req_ext -nodes -out /etc/certs/$IP.crt -keyout /etc/certs/$IP.key
    fi

    grep -q $IP /etc/nginx/sites-available/default-443-wlan0 || WIFIUPDATED=true
    if [ ${WIFIUPDATED} ]; then
        envsubst '${IP}' < /etc/nginx/sites-available/default.template > /etc/nginx/sites-available/default-443-wlan0 
        UPDATED=true
    fi
else
    rm -f /etc/nginx/sites-enabled/default-443-wlan0
fi

pidof nginx > /dev/null || UPDATED=true

[ ${UPDATED} ] && sudo service nginx restart && echo "Restarting nginx"
