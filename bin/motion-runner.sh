#!/bin/bash

VIDEO_DEVICE=$(ls /dev/video* | grep -v video0 | head -1)

if [ -n "$VIDEO_DEVICE" ] ; then
  echo "videodevice $VIDEO_DEVICE" > /home/streamer/.motion/camera0.conf
  /usr/bin/motion -c /home/streamer/.motion/motion.conf
else
  sleep 60
fi
