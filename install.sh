#!/bin-bash -e
# prepare environment for running growduino
# reruns of install.sh should change db password and django secret key
# multiple reruns must be non-destructive

echo -n "grdw" > /etc/hostname
hostname grdw

# install wanted packages
apt -y install postgresql-all mc motion python3-pip nginx gettext-base virtualenvwrapper python3-dev
apt -y install libffi-dev pkg-config at supervisor arduino-mk python3-requests
apt -y install libglib2.0-dev libgirepository1.0-dev libdbus-1-dev libcairo2-dev libzmq3-dev
apt -y install motion slay

rm /etc/nginx/sites-enabled/default

#set up database
pg_ctlcluster 11 main start

PGPASS=`openssl rand -base64 32`
PGUSER=gd3
DJANGO_SECRET=`openssl rand -base64 32`
LOGGER_PASS=`openssl rand -base64 32`

echo "CREATE USER ${PGUSER} WITH PASSWORD '${PGPASS}';" | sudo -u postgres psql
echo "ALTER USER ${PGUSER} WITH PASSWORD '${PGPASS}';" | sudo -u postgres psql
echo "CREATE DATABASE ${PGUSER} WITH OWNER ${PGUSER};" | sudo -u postgres psql

#set up main user
adduser --disabled-password --gecos "Grdw User" grdw
usermod -G dialout,netdev grdw

# set up streamer user
adduser --disabled-password --gecos "Streamer User" streamer
usermod -G video streamer

cp /root/utility-scripts/install-grdw.sh /tmp
sudo -u grdw bash /tmp/install-grdw.sh
echo "SECRET_KEY = '${DJANGO_SECRET}'" > ~grdw/local_settings/local_settings.py
echo "DB_PASS = '${PGPASS}'" >> ~grdw/local_settings/local_settings.py
echo "LOGGER_PASS = '${LOGGER_PASS}'" >> ~grdw/local_settings/local_settings.py
rsync -av /root/utility-scripts/django/ ~grdw/gd3_server/
chown -R grdw:grdw /home/grdw
cd /home/grdw/gd3_server
sudo -u grdw /home/grdw/.virtualenvs/gd3/bin/python ./manage.py migrate
if [ ! -e /home/grdw/gd3_server/core.json.loaded ]; then
    sudo -u grdw /home/grdw/.virtualenvs/gd3/bin/python ./manage.py loaddata core.json && touch /home/grdw/gd3_server/core.json.loaded
fi
sudo -u grdw /home/grdw/.virtualenvs/gd3/bin/python ./manage.py runscript core.scripts.create_logger_user
sudo -u grdw /home/grdw/.virtualenvs/gd3/bin/python /home/grdw/gd3_server/manage.py createsuperuser --username grdw --email djangoadmin@growduino
