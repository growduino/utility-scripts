import sys
import time
import os

""" restart supervisor or whole machine """

pin = 10


def pin2path(pin):
    return "/sys/class/gpio/gpio{pin}/value".format(pin=pin)


led_path = "/sys/class/leds/orangepi:red:status/trigger"

red_led_pin = 8
green_led_pin = 7


reset_counter = 0
doubleclick_counter = 0
doubleclick_thr = 12


def switch_led(state):
    if state is False:
        trigger = "none"
    else:
        trigger = "default-on"
    with open(led_path, "w") as led:
        led.write(trigger)
    with open(pin2path(red_led_pin), "w") as led:
        if trigger == "default-on":
            led.write("1")
        else:
            led.write("0")


def init_gpio(pin, mode="in"):
    try:
        with open("/sys/class/gpio/export", "w") as f:
            f.write(str(pin))
    except:
        pass
    with open("/sys/class/gpio/gpio{pin}/direction".format(pin=pin), "w") as f:
        f.write(mode)


def chime():
    switch_led(False)
    time.sleep(0.2)
    switch_led(True)
    time.sleep(0.2)
    switch_led(False)
    time.sleep(0.2)
    switch_led(True)
    time.sleep(0.2)
    switch_led(False)


def pin_read(pin):
    gpio_path = pin2path(pin)
    with open(gpio_path, "r") as btn:
        value = btn.readline().strip()
    return value == "1"


def pin_write(pin, what):
    with open(pin2path(pin), "w") as btn:
        btn.write(what)


def doubleclick_action():
    pass


if __name__ == "__main__":

    init_gpio(pin)
    init_gpio(green_led_pin, "out")
    init_gpio(red_led_pin, "out")
    switch_led(True)
    pin_write(green_led_pin, "1")

    time.sleep(0.1)
    switch_led(False)
    if not pin_read(pin):
        os.system("wall power.py not ready, button not connected?")
        time.sleep(60)
        sys.exit()

    while True:
        if pin_read(pin):
            if reset_counter > 0:
                switch_led(False)
                reset_counter = 0
            if doubleclick_counter > 0:
                doubleclick_counter = doubleclick_counter - 1
        else:
            if reset_counter == 0 and doubleclick_counter > 0:
                os.system("wall reset button double click, noop")
                doubleclick_action()
            reset_counter = reset_counter + 1
            doubleclick_counter = doubleclick_thr
            switch_led(True)
        time.sleep(0.1)

        if reset_counter > 0:
            os.system("wall reset counter: {}".format(reset_counter))

        if reset_counter == 12:
            os.system("wall reset by case button, rebooting")
            os.system("/root/utility-scripts/poweroff.sh")
            os.system("/sbin/poweroff")
