#!/bin/bash
/usr/sbin/service rsyslog stop
/usr/lib/armbian/armbian-ramlog stop
find /var/log.hdd -type f -delete
find /var/log -type f -delete
find /var/mail/ -type f -delete
find ~ -name "*hist*" -delete
rm ~/.viminfo
rm /home/grdw/.bash_history
rm -f /home/grdw/gd3_server/config.json.bak.*
rm /home/grdw/.viminfo
rm -rf /home/grdw/.vim
rm -rf /home/grdw/.local
rm -rf /home/grdw/.ipython
rm /var/cache/apt/archives/*deb
echo "Cleanup done, please reboot"
