#!/bin/bash
nohup /usr/bin/supervisorctl stop grdw:* webcam:webcamd >/dev/null &
nohup /usr/sbin/service postgresql stop >/dev/null &
nohup /usr/sbin/service nginx stop >/dev/null &
