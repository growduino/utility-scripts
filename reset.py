import sys
import time
import os
import requests
from shutil import copyfile
import pwd
import grp

from power import pin2path, led_path, switch_led, init_gpio, chime, pin_read, pin_write

""" restore default config """

pin = 9

reset_counter = 0
doubleclick_counter = 0
doubleclick_thr = 12


def doubleclick_action():
    switch_led(True)
    os.system("/usr/sbin/service supervisor restart")


if __name__ == "__main__":
    init_gpio(pin)
    switch_led(True)
    time.sleep(0.1)
    switch_led(False)
    if not pin_read(pin):
        os.system("wall reset.py not ready, button not connected?")
        time.sleep(60)
        sys.exit()

    while True:
        if pin_read(pin):
            if reset_counter > 0:
                switch_led(False)
                reset_counter = 0
            if doubleclick_counter > 0:
                doubleclick_counter = doubleclick_counter - 1
        else:
            if reset_counter == 0 and doubleclick_counter > 0:
                doubleclick_action()
            reset_counter = reset_counter + 1
            doubleclick_counter = doubleclick_thr
            switch_led(True)
        time.sleep(0.1)
        if reset_counter > 100:
            os.system("wall Restoring config")
            chime()
            reset_counter = 0
            os.system("/root/utility-scripts/settings-cleanup.sh")
