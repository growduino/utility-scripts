#!/bin/bash
if [ ! -L /etc/nginx/sites-enabled/default ]; then 
    rm -f /etc/nginx/sites-enabled/default
    ln -sf /etc/nginx/sites-available/default-allow-nossl /etc/nginx/sites-enabled/default
    service nginx restart
fi
if [ "$1" == "True" ]; then
    if  [ `readlink /etc/nginx/sites-enabled/default` != /etc/nginx/sites-available/default-redirect ]; then
        echo "updating: enforce ssl"
        ln -sf /etc/nginx/sites-available/default-redirect /etc/nginx/sites-enabled/default
        service nginx restart
    fi
fi

if [ "$1" == "False" ]; then
    if  [ `readlink /etc/nginx/sites-enabled/default` != /etc/nginx/sites-available/default-allow-nossl ]; then
        echo "updating: allow nossl"
        ln -sf /etc/nginx/sites-available/default-allow-nossl /etc/nginx/sites-enabled/default
        service nginx restart
    fi
fi
