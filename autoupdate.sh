#!/bin/bash -e
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
HOSTNAME=`hostname`

update_repo () {
    DIR=$1
    BRANCH=$2
        (cd $1 && git fetch && git reset --hard && git checkout $2 && git pull)
    }

check_library () {
    DIR=$2
    GIT_PATH=$1
    echo "Working on ${DIR}"

    if [ ! -d $DIR ]; then
        git clone $GIT_PATH $DIR
    fi
    (cd $DIR && git checkout master && git pull)
}

check_libraries () {
    grep -q gitlab ~/Arduino/libraries/OneWire/.git/config || rm -rf ~/Arduino/libraries/*
    cd ~/Arduino/libraries/
    check_library https://github.com/strange-v/MHZ19.git MHZ19
    check_library https://github.com/milesburton/Arduino-Temperature-Control-Library.git DallasTemperature
    check_library https://github.com/adafruit/Adafruit_BusIO.git Adafruit_BusIO
    check_library https://github.com/adafruit/Adafruit_Sensor.git Adafruit_Sensor
    check_library https://github.com/adafruit/Adafruit_BME280_Library.git Adafruit_BME280
    check_library https://gitlab.com/growduino/libs/BH1750.git BH1750
    check_library https://gitlab.com/growduino/libs/OneWire.git OneWire
    check_library https://gitlab.com/growduino/libs/SoftwareWire.git SoftwareWire
    check_library https://gitlab.com/growduino/libs/LiquidCrystal_I2C.git LiquidCrystal_I2C
    check_library https://gitlab.com/growduino/libs/dht.git dht
}

if [ ${HOSTNAME} = 'grdw' ]; then 
    echo -e "[${RED}Stopping supervisor${NC}]"
    sudo service supervisor stop
    echo -e "[${RED}Updating arduino firmware${NC}]"
    declare -x ARDMK_DIR="/usr/share/arduino"
    declare -x ARDUINO_DIR="/home/grdw/src/arduino"
    declare -x ARDUINO_SKETCHBOOK="/home/grdw/Arduino"
    declare -x BOARD="mega2560"

    check_libraries

    #hotfix bme280 migrating to BusIO not working with Arduino.mk
    (cd ~grdw/Arduino/libraries/Adafruit_BME280 && git checkout 0c4398f7194f57a1f7109650299c3b988739f49f )

    cd ~/Arduino/GrowduinoFirmware3/

    if git status -s -b |grep -q "## v2"; then
        echo -e "[${GREEN}v2 hardware${NC}]"
        update_repo ~/Arduino/GrowduinoFirmware3/ v2-master
        update_repo ~grdw/gd3_server/core/ v2-master
        update_repo ~grdw/gd3_server/core/static v2-master
    else
        echo -e "[${GREEN}v3 hardware${NC}]"
        update_repo ~/Arduino/GrowduinoFirmware3/ v3-master
        update_repo ~grdw/gd3_server/core/ v3-master
        update_repo ~grdw/gd3_server/core/static v3-master
    fi

    make clean upload
    echo -e "[${RED}Updating server${NC}]"
    cd ~grdw/gd3_server/gd3_server/ && sed -i.bak "s/^DEBUG =.*/DEBUG = False/" settings.py && sed -i.bak "s:STATIC_URL =.*:STATIC_URL = '/':" settings.py
    grep -q MEDIA_URL /home/grdw/gd3_server/gd3_server/settings.py || echo "MEDIA_URL = '/media/'" >> /home/grdw/gd3_server/gd3_server/settings.py
    cd ~/gd3_server/
    echo -e "[${RED}Updating frontend-pages${NC}]"
    echo -e "[${RED}Update python environment${NC}]"
    ~/.virtualenvs/gd3/bin/pip install --upgrade pip
    ~/.virtualenvs/gd3/bin/pip freeze | grep "django-basic-authentication-decorator==0.9" && ~/.virtualenvs/gd3/bin/pip uninstall -y django-basic-authentication-decorator
    ~/.virtualenvs/gd3/bin/pip install --upgrade -r core/environ/requirements.txt
    echo -e "[${RED}Migrate${NC}]"
    # sh ~/gd3_server/core/scripts/catch-migrations.sh
    ~/.virtualenvs/gd3/bin/python ./manage.py migrate
    ~/.virtualenvs/gd3/bin/python ./manage.py runscript core.scripts.trigger_fix
    #echo -e "[${RED}Running update root scripts${NC}]"
    #sudo /root/utility-scripts/update-scripts.sh
    echo -e "[${RED}Updating static files${NC}]"
    ~/.virtualenvs/gd3/bin/python ./manage.py collectstatic --clear --no-input
    echo -e "[${RED}Starting supervisor${NC}]"
    sudo service supervisor start
    echo -e "[${GREEN}All done${NC}]"
else
    echo -e "[${RED}Wrong hostname, just doing library setup${NC}]"
    check_libraries
fi
