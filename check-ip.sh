#!/bin/bash


NETIP=`ip addr show eth0 | grep "\<inet\>" | awk '{ print $2 }' | awk -F "/" '{ print $1 }'`
ETHSTATE=`cat /sys/class/net/eth0/operstate`

if [ -z ${NETIP} ]; then
    if [ ${ETHSTATE} == 'up' ]; then 
        sleep 10
        NETIP2=`ip addr show eth0 | grep "\<inet\>" | awk '{ print $2 }' | awk -F "/" '{ print $1 }'`
        if [ -z ${NETIP} ]; then
            echo "Restarting NM"
            /usr/sbin/service network-manager restart
        fi
        #    sleep 10 && /usr/bin/supervisorctl restart grdw:gunicorn
    fi
fi

sudo -u grdw /usr/local/bin/new-addr.sh
